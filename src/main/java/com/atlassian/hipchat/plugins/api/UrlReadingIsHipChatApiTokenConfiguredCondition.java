package com.atlassian.hipchat.plugins.api;

import com.atlassian.fugue.Effect;
import com.atlassian.hipchat.plugins.api.config.HipChatConfigurationManager;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.condition.UrlReadingCondition;
import com.atlassian.plugin.webresource.url.UrlBuilder;

import java.util.Map;

/**
 * CDN ready implementation of {@link IsHipChatApiTokenConfiguredCondition}. If the hipchat API token is configured
 * on the instance of JIRA or Confluence, links to css and js resources are generated with additional query parameter.
 *
 */
public class UrlReadingIsHipChatApiTokenConfiguredCondition implements UrlReadingCondition
{
    private static final String HIPCHAT_TOKEN_DEFINED = "hipchatToken";
    private final HipChatConfigurationManager hipChatConfigurationManager;

    public UrlReadingIsHipChatApiTokenConfiguredCondition(HipChatConfigurationManager hipChatConfigurationManager)
    {
        this.hipChatConfigurationManager = hipChatConfigurationManager;
    }


    @Override
    public void init(Map<String, String> stringStringMap) throws PluginParseException
    {
    }

    @Override
    public void addToUrl(final UrlBuilder urlBuilder)
    {
        hipChatConfigurationManager.getApiToken().foreach(new Effect<String>()
        {
            @Override
            public void apply(final String token)
            {
                urlBuilder.addToQueryString(HIPCHAT_TOKEN_DEFINED, "true");
            }
        });
    }

    @Override
    public boolean shouldDisplay(QueryParams queryParams)
    {
        final String hipchatToken = queryParams.get(HIPCHAT_TOKEN_DEFINED);
        return Boolean.parseBoolean(hipchatToken);
    }
}
