package com.atlassian.hipchat.plugins.api.client;

import com.google.common.base.Objects;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Date;

import static com.atlassian.hipchat.plugins.core.util.DateMapper.toDate;

public final class Room
{
    private final long id;
    private final String name;
    private final String topic;
    private final Date lastActive;
    private final Date created;
    private final long ownerId;
    private final boolean archived;
    private final boolean priv;
    private final String jid;

    @JsonCreator
    public Room(@JsonProperty("room_id") long id,
                @JsonProperty("name") String name,
                @JsonProperty("topic") String topic,
                @JsonProperty("last_active") long lastActive,
                @JsonProperty("created") long created,
                @JsonProperty("owner_user_id") long ownerId,
                @JsonProperty("is_archived") boolean archived,
                @JsonProperty("is_private") boolean priv,
                @JsonProperty("xmpp_jid") String jid)
    {
        this.id = id;
        this.name = name;
        this.topic = topic;
        this.archived = archived;
        this.priv = priv;
        this.jid = jid;
        this.lastActive = toDate(lastActive);
        this.created = toDate(created);
        this.ownerId = ownerId;
    }

    public long getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public String getTopic()
    {
        return topic;
    }

    public Date getLastActive()
    {
        return new Date(lastActive.getTime());
    }

    public Date getCreated()
    {
        return new Date(created.getTime());
    }

    public long getOwnerId()
    {
        return ownerId;
    }

    public boolean isArchived()
    {
        return archived;
    }

    public boolean isPrivate()
    {
        return priv;
    }

    public String getJid()
    {
        return jid;
    }

    @Override
    public String toString()
    {
        return Objects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .add("topic", topic)
                .add("last active", lastActive)
                .add("created", created)
                .add("owner id", ownerId)
                .toString();
    }
}
