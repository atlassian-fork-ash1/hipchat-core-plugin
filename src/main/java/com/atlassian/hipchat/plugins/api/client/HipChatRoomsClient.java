package com.atlassian.hipchat.plugins.api.client;

import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.util.concurrent.Promise;

import java.util.List;

import static com.atlassian.hipchat.plugins.api.client.Message.Format;
import static com.atlassian.hipchat.plugins.api.client.Message.Status;

public interface HipChatRoomsClient
{
    Promise<Either<ClientError, List<Room>>> list();

    Promise<Either<ClientError, Status>> message(String roomIdOrName,
                                                 String from,
                                                 String message,
                                                 Option<Format> format,
                                                 Option<Message.BackgroundColor> color,
                                                 Option<Boolean> notify);

    Promise<Either<ClientError, Room>> create(String name,
                                              long ownerId,
                                              Option<Boolean> privacy,
                                              Option<String> topic,
                                              Option<Boolean> enableGuestAccess);
}
