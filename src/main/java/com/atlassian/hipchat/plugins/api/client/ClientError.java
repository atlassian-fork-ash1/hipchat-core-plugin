package com.atlassian.hipchat.plugins.api.client;

import com.atlassian.fugue.Option;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;
import static com.google.common.base.Preconditions.checkNotNull;

public final class ClientError
{
    private final String message;
    private final Option<String> code;
    private final Option<String> type;
    private final Option<Throwable> exception;

    public ClientError(Throwable t)
    {
        this(checkNotNull(t).getMessage(), t);
    }

    public ClientError(String message, Throwable t)
    {
        this(message == null ? "Unexpected error" : message, noString(), noString(), some(checkNotNull(t)));
    }

    @JsonCreator
    public ClientError(@JsonProperty("message") String message, @JsonProperty("code") String code, @JsonProperty("type") String type)
    {
        this(checkNotNull(message), some(checkNotNull(code)), some(checkNotNull(type)), Option.<Throwable>none());
    }

    private ClientError(String message, Option<String> code, Option<String> type, Option<Throwable> exception)
    {
        this.message = checkNotNull(message);
        this.code = checkNotNull(code);
        this.type = checkNotNull(type);
        this.exception = checkNotNull(exception);
    }

    public String getMessage()
    {
        return message;
    }

    public Option<String> getCode()
    {
        return code;
    }

    public Option<String> getType()
    {
        return type;
    }

    public Option<Throwable> getException()
    {
        return exception;
    }

    private static Option<String> noString()
    {
        return none();
    }
}
