package com.atlassian.hipchat.plugins.core.client;

import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.hipchat.plugins.api.client.ClientError;
import com.atlassian.hipchat.plugins.api.client.HipChatRoomsClient;
import com.atlassian.hipchat.plugins.api.client.Message;
import com.atlassian.hipchat.plugins.api.client.Room;
import com.atlassian.httpclient.api.HttpClient;
import com.atlassian.util.concurrent.Promise;
import com.google.common.collect.ImmutableMap;
import org.codehaus.jackson.type.TypeReference;

import java.util.List;
import java.util.Map;

import static com.atlassian.hipchat.plugins.api.client.Message.Status;
import static com.atlassian.hipchat.plugins.core.client.ClientUtils.constantRight;
import static com.atlassian.hipchat.plugins.core.client.ClientUtils.parseJson;
import static com.atlassian.hipchat.plugins.core.client.ClientUtils.urlEncode;
import static com.google.common.base.Preconditions.checkNotNull;

public final class HipChatRoomsClientImpl implements HipChatRoomsClient
{
    private final HttpClient httpClient;

    public HipChatRoomsClientImpl(HttpClient httpClient)
    {
        this.httpClient = checkNotNull(httpClient);
    }

    @Override
    public Promise<Either<ClientError, List<Room>>> list()
    {
        return httpClient.newRequest("/rooms/list").get().<Either<ClientError, List<Room>>>transform()
                .ok(parseJson("rooms", new TypeReference<List<Room>>(){}))
                .others(ClientUtils.<List<Room>>error())
                .fail(ClientUtils.<List<Room>>throwable())
                .toPromise();
    }

    @Override
    public Promise<Either<ClientError, Status>> message(String roomIdOrName, String from, String message, Option<Message.Format> format, Option<Message.BackgroundColor> color, Option<Boolean> notify)
    {
        // TODO: validate params
        final Map<String, String> params = messageParametersAsMap(roomIdOrName, from, message, format, color, notify);

        return httpClient.newRequest("/rooms/message", "application/x-www-form-urlencoded", urlEncode(params))
                .post().<Either<ClientError, Status>>transform()
                .ok(constantRight(Status.SENT))
                .others(ClientUtils.<Status>error())
                .fail(ClientUtils.<Status>throwable())
                .toPromise();
    }

    @Override
    public Promise<Either<ClientError, Room>> create(String name, long ownerId, Option<Boolean> privacy, Option<String> topic, Option<Boolean> enableGuestAccess)
    {
        // TODO validate params
        final Map<String, String> params = createParametersAsMap(name, ownerId, privacy, topic, enableGuestAccess);

        return httpClient.newRequest("/rooms/create", "application/x-www-form-urlencoded", urlEncode(params))
                .post().<Either<ClientError, Room>>transform()
                .ok(parseJson("room", Room.class))
                .others(ClientUtils.<Room>error())
                .fail(ClientUtils.<Room>throwable())
                .toPromise();
    }

    private static ImmutableMap<String, String> createParametersAsMap(String name, long ownerId, Option<Boolean> privacy, Option<String> topic, Option<Boolean> enableGuestAccess)
    {
        final ImmutableMap.Builder<String, String> params = ImmutableMap.<String, String>builder()
                .put("name", name)
                .put("owner_user_id", String.valueOf(ownerId));
        if (privacy.isDefined())
        {
            params.put("privacy", privacy.get() ? "private" : "public");
        }
        if (topic.isDefined())
        {
            params.put("topic", topic.get());
        }
        if (enableGuestAccess.isDefined())
        {
            params.put("guest_access", enableGuestAccess.get() ? "1" : "0");
        }
        return params.build();
    }

    private static Map<String, String> messageParametersAsMap(String roomIdOrName, String from, String message, Option<Message.Format> format, Option<Message.BackgroundColor> color, Option<Boolean> notify)
    {
        final ImmutableMap.Builder<String, String> params = ImmutableMap.<String, String>builder()
                .put("room_id", roomIdOrName)
                .put("from", from)
                .put("message", message);

        if (format.isDefined())
        {
            params.put("message_format", format.get().value);
        }
        if (color.isDefined())
        {
            params.put("color", color.get().value);
        }
        if (notify.isDefined())
        {
            params.put("notify", notify.get() ? "1" : "0");
        }
        return params.build();
    }
}
