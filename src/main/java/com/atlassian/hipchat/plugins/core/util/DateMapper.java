package com.atlassian.hipchat.plugins.core.util;

import java.util.Date;

public class DateMapper
{
    public static Date toDate(long unixTime)
    {
        return new Date(unixTime * 1000);
    }
}
